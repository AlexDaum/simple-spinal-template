package template

import spinal.core._

// Hardware definition
case class TopLevel() extends Component {
  val io = new Bundle {

  }

}

object TopLevelVerilog extends App {
  Config.spinal.generateVerilog(TopLevel())
}

object TopLevelVhdl extends App {
  Config.spinal.generateVhdl(TopLevel())
}
